import { createAttribute } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { charAtIndex } from 'pdf-lib';
import {
  ListService,
  LocalService,
  AladinService,
  HttpService,
} from 'src/app/core';
import { fabric } from 'fabric';
declare var require: any;
var myalert = require('sweetalert2');
var $ = require('jquery');
@Component({
  selector: 'app-vetement',
  templateUrl: './vetement.component.html',
  styleUrls: ['./vetement.component.scss'],
})
export class VetementComponent implements OnInit {
  cltobj: any = [];
  head = true;
  file: any;
  imagepreview: any;
  CacheCrea = false;
  cacheClothes = true;
  details: any = {};
  showdetail = false;
  models: any;
  url = '/editor/cloth/';
  obj: any = [];
  cart = false;
  canvas: any;
  products: any;
  produit: any = [];
  data =
    'Vêtement : Aladin vous propose des vêtements personnalisés de très bonne qualité avec des finitions qui respectent vos goûts. Vous serez fière de les porter partout quel que soit le lieu et l’événement.';
  constructor(
    private p: ListService,
    private local: LocalService,
    private uplod: AladinService,
    private http: HttpService
  ) {}

  ngOnInit(): void {
    //$("body").children().first().before($(".modal"));

    this.http.get().subscribe(
      (res) => {
        console.log(res);
        this.models = res;
        for (let item of this.models) {
          console.log(item);
          //this.models[this.models.indexOf(item)].obj= JSON.parse(item.obj);

          this.models[this.models.indexOf(item)].description = JSON.parse(
            item.description
          );

          this.getCanvasUrl(this.models[this.models.indexOf(item)].obj, item)
            .then(async (res) => {
              await setTimeout(() => {}, 1000);
            })
            .catch((err) => {
              console.log(err);
            });
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  async getCanvasUrl(obj: any, item: any) {
    let canvas = new fabric.Canvas(null, {
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor: 'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful: true,
      stopContextMenu: false,
    });

    console.log(item);
    return await canvas.loadFromJSON(obj, (ob: any) => {
      canvas.setHeight(400);
      canvas.setWidth(400);
      var product = {
        url: canvas.toDataURL(),
        //url2:canvas.toDataURL(),
        price: item.description.price,
        promo: item.description.promo,
        size: item.description.size,
        type: item.description.type,
        name: item.description.name,
        owner: item.description.owner,
        comment: item.description.made_with,
        item: obj,
        width: item.width,
        height: item.height,
      };
      if (item.category == '1') {
        this.produit.push(product);
        //this.products.push(product)
      }
    });
  }
  Upload(event: any) {
    let file = event.target.files[0];
    if (!this.uplod.UpleadImage(file)) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imagepreview = reader.result;
      };

      reader.readAsDataURL(file);
      this.affichecrea();
      //let modal=document.getElementById('modalbtn')
      //this.p.triggerMouse(modal)
      console.log(event);
    } else {
    }
  }

  showDetails(data: any) {
    console.log(data);

    Object.assign(this.details, {
      url: data.url,
      made_with: data.made_with,
      price: data.price,
      name: data.name,
      size: data.size,
      show: true,
      item: data.item,
      width: data.width,
      height: data.height,
    });
    console.log(this.details);
    this.CacheCrea = false;
    this.cacheClothes = false;
    this.head = true;
  }

  affichecrea() {
    this.CacheCrea = true;
    this.cacheClothes = false;
  }

  getComponent(value: boolean) {
    this.cacheClothes = value;
    this.CacheCrea = !this.CacheCrea;
  }

  ChangeComponent(value: boolean) {
    this.cacheClothes = value;
  }
}
