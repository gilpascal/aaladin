import { TestBed } from '@angular/core/testing';

import { NeweditorService } from './neweditor.service';

describe('NeweditorService', () => {
  let service: NeweditorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NeweditorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
