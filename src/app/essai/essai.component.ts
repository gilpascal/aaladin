import { Component, OnInit, OnChanges,ViewChild } from '@angular/core';
import { fabric } from 'fabric';
import { install } from 'chart-js-fabric';
install(fabric);
import {  ActivatedRoute } from '@angular/router';
import { AddtextService,TextfontService,AddshapeService,ListService,LocalService } from '../core';
declare var require: any
var FontFaceObserver = require('fontfaceobserver');
var myalert=require('sweetalert2');
import { ColorEvent } from 'ngx-color';
var $ = require("jquery");
import { ContextMenuComponent } from 'ngx-contextmenu';
fabric.Canvas.prototype.selectionDashArray 
@Component({
  selector: 'app-essai',
  templateUrl: './essai.component.html',
  styleUrls: ['./essai.component.scss']
})
export class EssaiComponent implements OnInit,OnChanges {
hide=false
hide1=false
hide2=false
isp:boolean =true;
    canvas:any;
    rect:any;
    s:any;
    face1:any;
    dt:any;
    red:any;
    is_underlined=false;
    backtext:any=[];
    shapes:any;
    selectedDay="blue";
    isCliked=false;
    isbold:boolean =false;
    Text:any;
    url:any;
    stroke_size=1;
    strokecolor="#0B44A5"
    Textcolor="#0B44A5";
    stroke_color="blue"
    prod_color="";
    colorarray=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"blue","yellow","orange","blacK","red","indigo","green","brown","#800080","#808000","#000080"];
    name:any;
    inpunmb=1;
    divider=true
    text_height:any;
    text_width:any;
    imgsrc:any;
    selecetdFile:any;
    public fontFace: any;
    imagePreview:any;
    fonts = ["Choix de police","Arial","roboto",'Tangerine','caveat',"poppins","Dancing Script","cinzel","Festive","satisfy","Yellowtail","pacifico","monoton","Oleo Script","Pinyon Script"];
    designed:any=[];
    price:any;
    para:any;
    logo:any;
    prodid:any;
    iscat:any;
    Image:any;
    show=false;
    active=false;
    face_is_active=false;
    pback:any;
    pfront:any;
    colors:any=["white","red"];

    @ViewChild(ContextMenuComponent) public basicMenu:any= ContextMenuComponent;
  constructor(private Url:TextfontService,private add:AddtextService,private route: ActivatedRoute,private shp:AddshapeService,private p:ListService,private L:LocalService) { }
  
  ngOnChanges():void{
    if(localStorage.getItem('price')){
      this.price=localStorage.getItem('price');
    }

  }
  ngOnInit(): void {
      this.canvas = new fabric.Canvas('aladin-editor',{ 
        hoverCursor: 'pointer',
        selection: true,
        selectionBorderColor:'blue',
        fireRightClick: true,
      });
     
     
      this.canvas.filterBackend=new fabric.WebglFilterBackend();
      this.canvas.on('mouse:moving', this.onMouseMove)
      this.name=this.route.snapshot.paramMap.get('id');
      this.para =this.route.snapshot.paramMap.get('name');
     
     
     
     let text_h= document.getElementById("Range3")
     let text_w =document.getElementById("customRange3")
     
     text_h?.addEventListener('input',()=>{
      let text = this.canvas.getActiveObject();
      if(text && text.type=="activeSelection"){
        for(let el of text._objects){
          el.set({fontSize:this.text_height,height:this.text_height});
          this.canvas.renderAll(el)
     
        }
     
      }
      if(text && text.type!="activeSelection"){
        text.set({fontSize:this.text_height,height:this.text_height});
        this.canvas.renderAll(text)
      }
      this.canvas.requestRenderAll();
     
     });
     
     
     
     document.getElementById('strokesize')?.addEventListener('input',()=>{
      if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().type=="activeSelection"){
        for(let el of this.canvas.getActiveObject()._objects){
          el.set({strokeWidth:this.stroke_size,stroke:this.stroke_color});
          this.canvas.renderAll(el);
        }
      }
      if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().type!="activeSelection"){
        this.canvas.getActiveObject().set({strokeWidth:this.stroke_size,stroke:this.stroke_color});
        this.canvas.renderAll(this.canvas.getActiveObject());
      }
      this.canvas.requestRenderAll();
     });
     
     text_w?.addEventListener("input",()=>{
      let text=this.canvas.getActiveObject();
      if(text && text.type!="activeSelection"){
        text.set({fontSize:this.text_width,width:this.text_width});
        this.canvas.renderAll(text);
     
      }
     
      if(text && text.type=="activeSelection"){
        for(let item of text._objects){
          item.set({fontSize:this.text_width,width:this.text_width});
          this.canvas.renderAll(item);
     
        }
      }
      this.canvas.requestRenderAll();
     });
     
     if(this.para=="cloth"){
      this.iscat=this.para;
      this.prodid=this.name;
      this.p.getcloth(this.name).subscribe(res=>{
        this.dt=res;    
        for(let elt of this.dt){
         this.price=elt.price;
         this.s=elt.img;
         this.face1=elt.back_side;
         if(this.colors.indexOf(elt.lib)==-1){
          this.colors.push(elt.lib);
     
         }
         localStorage.setItem('price',elt.price);
         if(+elt.is_back==0){
          this.imgsrc=elt.front_side;
          
          this.makeImage(this.imgsrc);
          this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
         }else{
           this.backtext.push(elt.text)
         }
         
        }
      },
      err=>{console.log(err)}
      );
     }
     
     if(this.para=="gadgets"){
      this.iscat=this.para;
      this.prodid=this.name;
      this.p.getGadget(this.name).subscribe(
        res=>{
        this.dt=res;
        for(let elt of this.dt){
         this.price=elt.price;
         this.s=elt.img;
         this.face1=elt.back_side;
         if(this.colors.indexOf(elt.lib)==-1){
          this.colors.push(elt.lib);
     
         }
          if(+elt.is_back==0){
          this.imgsrc=elt.front_side;
          localStorage.setItem('price',elt.price);
          this.makeImage(this.imgsrc);
          this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
         }
        
        }
     
      },
      err=>{console.log(err)}
      );
     }
     
     let ofont:any=[]
     this.p.getfonts().subscribe(
       res=>{
         ofont=res
         if(ofont.status==200){
           for(let item of ofont.fonts){
             this.fonts.push(item.name);
             this.p.addStylesheetURL(item.url);
           }
           this.fonts.unshift('Times New Roman');
           var select = document.getElementById("family");
           this.fonts.forEach(function(font) {
           var opt = document.createElement('option');
            opt.innerHTML=font;
            opt.value=font;
            select?.appendChild(opt);
              });
              var select = document.getElementById("font-family");
              this.fonts.forEach(function(font) {
              var opt = document.createElement('option');
               opt.innerHTML=font;
               opt.value=font;
               select?.appendChild(opt);
              });
         }
       },
       err=>{
         console.log(err)
       }
     )
     
     if(this.para=="disps"){
      this.iscat=this.para;
      this.prodid=this.name;
      this.p.getDisp(this.name).subscribe(res=>{
        this.dt=res;
        for(let elt of this.dt){
         this.price=elt.price;
         this.s=elt.img;
         if(this.colors.indexOf(elt.lib)==-1){
          this.colors.push(elt.lib);
     
         }
         this.face1=elt.back_side;
         localStorage.setItem('price',elt.price);
         if(+elt.is_back==0){
          this.imgsrc=elt.front_side;
          this.makeImage(this.imgsrc);
          this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
        
        }
        
        }
      },
      err=>{console.log(err)}
      );
     }
     
     
     if(this.para=="prints"){
      this.iscat=this.para;
      this.prodid=this.name;
      this.p.getprint(this.name).subscribe(res=>{
        this.dt=res;
        for(let elt of this.dt){
         this.price=elt.price;
         this.s=elt.img;
         if(this.colors.indexOf(elt.lib)==-1){
          this.colors.push(elt.lib);
         }
         this.face1=elt.back_side
         localStorage.setItem('price',elt.price)
         if(+elt.is_back==0){
          this.imgsrc=elt.front_side;
          this.makeImage(this.imgsrc);
          this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
         }
         
        }
      },
      err=>{console.log(err)}
      );
     }
     
     if(this.para=="packs"){
      this.iscat=this.para;
      this.prodid=this.name;
      this.p.getPack(this.name).subscribe(res=>{
        this.dt=res
        for(let elt of this.dt){
         this.s=elt.front_side;
         this.price=elt.price;
         this.face1=elt.back_side;
         if(this.colors.indexOf(elt.lib)==-1){
          this.colors.push(elt.lib);
     
         }       localStorage.setItem('price',elt.price)
         if(+elt.is_back==0){
          this.imgsrc=elt.front_side;
          this.makeImage(this.imgsrc);
          this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
         }
         
        }
      },
      err=>{console.log(err)}
      );
     }
     if(this.para=="tops"){
      this.iscat=this.para;
      this.prodid=this.name;
      this.p.gettop(this.name).subscribe(res=>{
        this.dt=res;
        for(let elt of this.dt){
         this.imgsrc=elt.front_side;
         this.s=elt.img;
         this.price=elt.price;
         localStorage.setItem('price',elt.price)
         this.makeImage(this.imgsrc);
     
     
        }
      },
      err=>{console.log(err)}
      );
     }
     
     }

  
  
 cache(){
  this.hide=!this.hide
  this.hide1=false
 }
cache1(){
  this.hide1=!this.hide1
  this.hide=false
}
cache2(){
  this.hide2=!this.hide2
  this.hide1=false
  this.hide=false
}


texteclor($event:ColorEvent){
  let color = $event.color.hex;   
   let item= this.canvas.getActiveObject();

   if(item && item.type!="activeSelection"){
     item.set({fill:color});
     this.canvas.renderAll(item);
   }

   if(item && item.type=="activeSelection"){
     for(let el of item._objects){
       el.set({fill:color});
       this.canvas.renderAll(el);
     }
    }
 this.canvas.requestRenderAll();
}


setStroke($event:ColorEvent){
let color=$event.color.hex;
let item =  this.canvas.getActiveObject();
if(item && item.type!="activeSelection"){
 item.set({strokeWidth:this.stroke_size,stroke:color});
 this.canvas.renderAll(item);
}
if(item && item.type=="activeSelection"){
 for(let el of item._objects){
   el.set({strokeWidth:this.stroke_size,stroke:color});
   this.canvas.renderAll(el);
 }
}
 this.canvas.requestRenderAll();
}

changeComplete($event:ColorEvent){
   console.log($event.color.hex);
   let image;
   for(let elt of this.dt){
     for(let color of this.colors){
       if($event.color.hex==color){
         image=elt.lib_img;
       break;
     // this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
       }

     }    
     
    }

    if(image!=undefined){
     this.makeImage(image);
   }

 }


 toggle(){
   this.show=!this.show;
 }


 OnRightClick(event:any){   
   let item =this.canvas.getActiveObject();
   if(item){
     var elt=document.getElementById('xdv')
     this.p.triggerMouse(elt)   
   }
   
   return false
 }


 


underline(){
let item=this.canvas.getActiveObject()
if(item && item.type!="activeSelection"){
 if(!item.underline){
   item.set({underline:true});
   this.canvas.renderAll(item);

 }else{
   item.set({underline:false});
   this.canvas.renderAll(item);


 }
}

if(item && item.type=="activeSelection"){

 for(let el of item._objects){
   if(!item.underline){
     el.set({underline:true});
     this.canvas.renderAll(el);

   }else{
     el.set({underline:false});
     this.canvas.renderAll(el);
   }
 }


}
this.canvas.requestRenderAll();

}

turnImage(event:any){
let filters=this.canvas.backgroundImage.filters
var color;
var objt=this.canvas.getObjects()
for(let i=0;i<objt.length;i++){
this.canvas.remove(objt[i]);
}
if( filters[0]!=undefined){
color=filters[0].color


}else{
color =""
}
console.log(color);
if(event.target.id=="back"){
console.log(event.target.id)
//this.pback=this.canvas.toDataURL();
for(let elt of this.dt){
 localStorage.setItem('price',elt.price);
 if(+elt.is_back==1){
   this.imgsrc=elt.back_side;
   this.makeImage(this.imgsrc,color);
   this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);

 }

}
// this.canvas.backgroundImage.filters=filters;

}

if(event.target.id=="front"){
console.log(event.target.id)
//this.pfront=this.canvas.toDataURL();
for(let elt of this.dt){
 this.price=elt.price;
 localStorage.setItem('price',elt.price);
 if(+elt.is_back==0){
   this.imgsrc=elt.front_side;
   this.makeImage(this.imgsrc,color);
   this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
 }

}
//this.canvas.backgroundImage.filters=filters;
}

}

 selectChangeHandler(event: any) {
   let item= this.canvas.getActiveObject();
   this.Text=item;
   this.loadfont(event.target.value);
   if(this.Text && this.Text.type=="activeSelection"){
     for(let el of this.Text._objects){
       el.set({
         fontFamily:event.target.value
       });
       this.canvas.renderAll(el);
       this.canvas.requestRenderAll();

     }

   }

   if(this.Text && this.Text.type!="activeSelection"){
     this.Text.set({
       fontFamily:event.target.value
     });
     this.canvas.renderAll(this.Text);
     this.canvas.requestRenderAll();
   }
  

 }

 setTextBeforeEdit(){
   let text = this.canvas.getActiveObject()
   if(text && text.type!="activeSelection"){
     
     if(text.underline && text._textBeforeEdit){
       text.set({text:text._textBeforeEdit,underline:false});
       this.canvas.renderAll(text);

     }   
     if(text._textBeforeEdit){
       text.set({text:text._textBeforeEdit});
       this.canvas.renderAll(text);

     }
   } 

   if(text && text.type=="activeSelection"){
     for(let el of text._objects){
       if(el.underline && el._textBeforeEdit){
         el.set({text:el._textBeforeEdit,underline:false});
         this.canvas.renderAll(el);

       }   
       if(el._textBeforeEdit){
         el.set({text:el._textBeforeEdit});
         this.canvas.renderAll(el);
       }

     }
   }
   this.canvas.requestRenderAll();

 }

 removeItem(){
   let item = this.canvas.getActiveObject();
   if(item && item.type!="activeSelection"){
     this.canvas.remove(item);
   }

   if(item && item.type=="activeSelection"){
     for(let e of item._objects){
       this.canvas.remove(e);
     }
   }

 }
 

 MakeItalic(event:any){
   let item= this.canvas.getActiveObject();
   console.log(this.canvas);
   this.Text=item;
   if(this.Text!=undefined && this.Text.type=="activeSelection"){
     for(let el of this.Text._objects){
       if(el.fontStyle!="normal"){
         el.set({fontStyle:'normal'});
         this.canvas.renderAll(el)
        
       }else{
         el.set({fontStyle:'italic'});
         this.canvas.renderAll(el);
       }
     }
     
   }

   if(this.Text && this.Text!="activeSelection"){
     if(this.Text.fontStyle!="normal"){
       this.Text.set({fontStyle:"normal"});
       this.canvas.renderAll(this.Text);
     }else{
       this.Text.set({
         fontStyle:"italic"
       });
       this.canvas.renderAll(this.Text);
     }
   }
   this.canvas.requestRenderAll();

 }

 MakeBold(event:any){
   let item= this.canvas.getActiveObject();
   this.Text=item;  
   if(this.Text!=undefined && this.Text.type=="activeSelection"){    
       for(let el of this.Text._objects){
         if(el.fontWeight=="normal"){
         el.set({fontWeight:'bold'});
         this.canvas.renderAll(el);
       }else{
           el.set({fontWeight:'normal'});
           this.canvas.renderAll(el);        
       }
     }
     this.canvas.requestRenderAll();  
   }
     
   if(this.Text!=undefined && this.Text.type!="activeSelection"){
     if(this.Text.fontWeight=="normal"){   
       this.Text.set({fontWeight:'bold'});
         this.canvas.renderAll(this.Text);
         this.canvas.requestRenderAll();
      
     }else{
         this.Text.set({fontWeight:'normal'});
         this.canvas.renderAll(this.Text);
         this.canvas.requestRenderAll();
       
     }

   }
 }


 changeTextColor(event:any){
   this.selectedDay = event.target.value;
   let item= this.canvas.getActiveObject();
   if(item.type=="activeSelection"){
     console.log(item._objects);
     for(let el of item._objects){
       el.set({fill:this.selectedDay});
       this.canvas.renderAll(el);
     }
   }
   else{
     item.set({fill:this.selectedDay});
     this.canvas.renderAll(item);
   } 
   this.canvas.requestRenderAll();
 }


 setImage(event:any){
   this.colors=["white","red"];
   let id=event.target.id;
   let m=event.target.name;
   var objt=this.canvas.getObjects()
   for(let i=0;i<objt.length;i++){
     this.canvas.remove(objt[i]);
   }
   this.canvas.renderAll()
   if(id!=undefined&&m!=undefined){
     //vetements
     if(m=="cloth"){
       this.iscat=m;
        this.prodid=id;
        this.p.getcloth(id).subscribe(res=>{
       this.dt=res;    
       for(let elt of this.dt){
       this.price=elt.price;
       this.s=elt.front_side;
       this.face1=elt.back_side;
       if(this.colors.indexOf(elt.lib)==-1){
         this.colors.push(elt.lib);
 
        }
       localStorage.setItem('price',elt.price);
       if(+elt.is_back==0){
         this.imgsrc=elt.front_side;
         this.makeImage(this.imgsrc);
         this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
       }
    
     }
     var elt=document.getElementById('closept');
     this.p.triggerMouse(elt); 

 },


 err=>{console.log(err);}
 );

 }
  //gadgets
   if(m=="gadgets"){
     this.iscat=m;
     this.prodid=id;
 
     this.p.getGadget(id).subscribe(res=>{
       this.dt=res;
       for(let elt of this.dt){
        this.price=elt.price;
        this.s=elt.front_side;
        this.face1=elt.back_side;
        if(this.colors.indexOf(elt.lib)==-1){
         this.colors.push(elt.lib);
 
        }
        if(+elt.is_back==0){
         this.imgsrc=elt.front_side;
         this.makeImage(this.imgsrc);
         this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
        }
       
        localStorage.setItem('price',elt.price);
       }
     var elt=document.getElementById('closept');
     this.p.triggerMouse(elt); 
     },
     err=>{console.log(err)}
     );
    }
 
    //disps
    if(m=="disps"){
     this.iscat=m;
     this.prodid=id;
 
     this.p.getDisp(id).subscribe(res=>{
       this.dt=res;
       for(let elt of this.dt){
        this.price=elt.price;
        this.s=elt.front_side;
        this.face1=elt.back_side;
        if(this.colors.indexOf(elt.lib)==-1){
         this.colors.push(elt.lib);
 
        }    
        localStorage.setItem('price',elt.price)
        if(+elt.is_back==0){
         this.imgsrc=elt.front_side;
         this.makeImage(this.imgsrc);
         this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
        }
        
       }
       var elt=document.getElementById('closept');
     this.p.triggerMouse(elt); 
     },
     err=>{console.log(err);}
     );
    }
    //packs
    if(m=="packs"){
     this.iscat=m;
     this.prodid=id;
     this.p.getPack(id).subscribe(res=>{
       this.dt=res
       for(let elt of this.dt){
        this.s=elt.front_side;
        this.price=elt.price;
        this.face1=elt.back_side;
        if(this.colors.indexOf(elt.lib)==-1){
         this.colors.push(elt.lib);
 
        }           localStorage.setItem('price',elt.price)
        if(+elt.is_back==0){
         this.imgsrc=elt.front_side;
         this.makeImage(this.imgsrc);
         this.makeText(elt.fill,elt.fontfamily,elt.text,elt.y_value,elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
        }
        
       }
       var elt=document.getElementById('closept');
     this.p.triggerMouse(elt); 
     },
     err=>{console.log(err)}
     );
    }
    //printed
    if(m=="prints"){
     this.iscat=m;
     this.prodid=id;
     this.p.getprint(id).subscribe(res=>{
       this.dt=res;
       for(let elt of this.dt){
        this.price=elt.price;
        this.s=elt.front_side;
        this.face1=elt.back_side;
        if(this.colors.indexOf(elt.lib)==-1){
         this.colors.push(elt.lib);
 
        }
 
        localStorage.setItem('price',elt.price);
        if(+elt.is_back==0){
         this.imgsrc=elt.front_side;
         this.makeImage(this.imgsrc);
         this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
        }
       }
       var elt=document.getElementById('closept');
     this.p.triggerMouse(elt); 
     },
     err=>{console.log(err);}
     );
    }


   }

 }

makeImage(src:any,mcolor:string =""):any{
   var img = new Image();
   let canvas= this.canvas;
   img.src=src
   img.crossOrigin = 'anonymous';
   this.Image=img;
   if(this.show!=true){

            img.onload = function() {
             var f_img = new fabric.Image(img);
         if(mcolor !="" && mcolor != undefined){
           f_img.filters?.push(new fabric.Image.filters.BlendColor({
             color:mcolor, 
             mode: 'tint',
             alpha:0.7
         }));
         f_img.applyFilters()
   
         }
             canvas.setBackgroundImage(f_img,canvas.renderAll.bind(canvas), {
                 backgroundImageOpacity: 0.1,
                 backgroundImageStretch: false,
                 left:5,
                 top:5,
                 right:5,
                 bottom:5,
                 height:600,
                 width:600,
             });
             canvas.centerObject(f_img);
             canvas.requestRenderAll();


     }

   }else{

     img.onload = function() {
       var f_img = new fabric.Image(img);

   if(mcolor !="" && mcolor !=undefined ){
     f_img.filters?.push(
       new fabric.Image.filters.BlendColor({
       color:mcolor, 
       mode: 'tint',
       alpha:0.1
   }));
   f_img.applyFilters();

   }
 
       canvas.setBackgroundImage(f_img,canvas.renderAll.bind(canvas), {
           backgroundImageOpacity: 0.1,
           backgroundImageStretch: false,
           left:5,
           top:5,
           right:5,
           bottom:5,
           height:600,
           width:600,

       });
       canvas.centerObject(f_img);
       canvas.requestRenderAll();


     }
}
}





createText(){
this.loadfont("pacifico");
this.makeText('blue',"normal",'Je personnalise',300,200,50,300," ",0);
}

makeText(fill:any,font:any,text:string,top:number,left:number,height:number,width:number,strok:string,strokwidth:number){
   this.Text= new fabric.IText(text,{
     top:top,
     left:left,
     fill:fill,
     fontStyle:'normal',
     cornerStyle:'circle',
     fontFamily:font,
 
   });
   console.log(height,width,strok);
   this.Text.set({width:width,height:height,stroke:strok,strokWidth:strokwidth})
   this.canvas.add(this.Text).setActiveObject(this.Text);
   this.canvas.renderAll(this.Text);
   this.canvas.requestRenderAll();
   
}



setLogo(event:any){
var imgInstance = new fabric.Image(event.target, {
left: 200,
top: 100,
width:300,
height:300
});
this.canvas.add(imgInstance).setActiveObject(imgInstance);
this.canvas.requestRenderAll();

this.logo=imgInstance;
}

onFileUpload(event:any){
this.selecetdFile = event.target.files[0];
const reader = new FileReader();
reader.onload = () => {
this.imagePreview = reader.result;
};
reader.readAsDataURL(this.selecetdFile);

}


getPrice(){
 let price=localStorage.getItem('price')
   this.inpunmb=+this.inpunmb+1;
   if(this.inpunmb>0&&price!=null){
     this.price= this.inpunmb*(+price);
   }
 }

getPricem(){
let price=localStorage.getItem('price')
this.inpunmb=this.inpunmb-1;
if(this.inpunmb>0 && price!=null){
 if(((+this.inpunmb)*(+this.price))>0){
   this.price= ((+this.inpunmb)*(+price));
 }

}else{
 this.inpunmb=this.inpunmb+1;

}
 }


addtocart(){
 let err=false
 this.getback_info();
 this.getfront_info();
 let cart_data={
   in_stock:"En stock",
   qty:this.inpunmb,
   price:localStorage.getItem('price'),
   t:this.price
 };
 if(this.Text || this.logo){
   this.url=this.canvas.toDataURL();
   Object.assign(cart_data,{img:this.url})
   this.L.adtocart(cart_data);
   if(!err){
     myalert.fire({
       title:'<strong>produit ajouté</strong>',
       icon:'success',
       html:
         '<h6 style="color:blue">Felicitation</h6> ' +
         '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
         '<a href="/cart">Je consulte mon panier</a> ' 
         ,
       showCloseButton: true,
       showCancelButton: true,
       focusConfirm: false,
       confirmButtonText:
         '<i class="fa fa-thumbs-up"></i> like!',
       confirmButtonAriaLabel: 'Thumbs up, great!',
       cancelButtonText:
         '<i class="fa fa-thumbs-down"></i>',
       cancelButtonAriaLabel:'Thumbs down'
     })
   }

 }else{
   err=!err
   if(err){
     myalert.fire({
       icon: 'error',
       title: 'erreur...',
       text: 'veillez personnaliser le produit!',
     })
   }
 }


}

loadfont(f:any){
 let fonts=new FontFaceObserver(f);
 fonts.load().then(function () {

 }).catch((err:any)=>{
   console.log(err);
 });
}



Copy() {
let canvas=this.canvas;

// clone what are you copying since you
// may want copy and paste on different moment.
// and you do not want the changes happened
// later to reflect on the copy.
canvas.getActiveObject().clone(function(cloned:any) {
 canvas._clipboard= cloned;
});
}


Paste() {
let canvas=this.canvas
// clone again, so you can do multiple copies.
canvas._clipboard.clone(function(clonedObj:any) {
 canvas.discardActiveObject();
 clonedObj.set({
   left: clonedObj.left + 15,
   top: clonedObj.top + 15,
   evented: true,
 });
 if (clonedObj.type === 'activeSelection') {
   // active selection needs a reference to the canvas.
   clonedObj.canvas = canvas;
   clonedObj.forEachObject(function(obj:any) {
     canvas.add(obj);
   });
   // this should solve the unselectability
   clonedObj.setCoords();
 } else {
   canvas.add(clonedObj);
 }
 canvas._clipboard.top += 15;
 canvas._clipboard.left += 15;
 canvas.setActiveObject(clonedObj);
 canvas.requestRenderAll();
});
}

getback_info(){
console.log(this.face1)
}

getfront_info(){
console.log(this.s)


}

onMouseMove(){
console.log(this.canvas.backgroundImage);

}
}
