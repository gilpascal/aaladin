import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ImprimeComponent } from './imprime/imprime.component';

const routes: Routes = [
  {path:'', component:ImprimeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImprimerRoutingModule { }
