import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AladintshirtsRoutingModule } from './aladintshirts-routing.module';
import { SharedModule } from 'src/app/shared';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AladintshirtsRoutingModule,
    SharedModule
  ]
})
export class AladintshirtsModule { }
