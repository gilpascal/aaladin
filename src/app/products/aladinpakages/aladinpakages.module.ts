import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AladinpakagesRoutingModule } from './aladinpakages-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AladinpakagesRoutingModule
  ]
})
export class AladinpakagesModule { }
