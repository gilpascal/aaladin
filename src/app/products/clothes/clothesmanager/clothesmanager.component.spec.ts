import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClothesmanagerComponent } from './clothesmanager.component';

describe('ClothesmanagerComponent', () => {
  let component: ClothesmanagerComponent;
  let fixture: ComponentFixture<ClothesmanagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClothesmanagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClothesmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
