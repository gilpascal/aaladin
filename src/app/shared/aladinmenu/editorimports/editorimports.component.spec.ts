import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorimportsComponent } from './editorimports.component';

describe('EditorimportsComponent', () => {
  let component: EditorimportsComponent;
  let fixture: ComponentFixture<EditorimportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditorimportsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorimportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
