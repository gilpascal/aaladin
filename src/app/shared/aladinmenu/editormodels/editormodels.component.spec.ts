import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditormodelsComponent } from './editormodels.component';

describe('EditormodelsComponent', () => {
  let component: EditormodelsComponent;
  let fixture: ComponentFixture<EditormodelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditormodelsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditormodelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
