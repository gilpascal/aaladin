import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorheadComponent } from './editorhead.component';

describe('EditorheadComponent', () => {
  let component: EditorheadComponent;
  let fixture: ComponentFixture<EditorheadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditorheadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
