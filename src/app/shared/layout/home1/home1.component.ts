import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
import { fabric } from 'fabric';
@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.scss']
})
export class Home1Component implements OnInit {
   tops:any;
   clothes:any
   produit: any = [];
   url='/editor/tops/'
  constructor(private l : ListService) { }

  ngOnInit(): void {
    this.l.getclothpage().subscribe(
      res=>{
        this.clothes = res.data
        console.log(this.clothes);
        for (let item of this.clothes) {
          console.log(item);
          //this.models[this.models.indexOf(item)].obj= JSON.parse(item.obj);

          this.clothes[this.clothes.indexOf(item)].description = JSON.parse(
            item.description
          );

          this.getCanvasUrl(this.clothes[this.clothes.indexOf(item)].obj, item)
            .then(async (res) => {
              await setTimeout(() => {}, 1000);
            })
            .catch((err) => {
              console.log(err);
            });
        }
      
      },
      err=>{
        console.log(err)
      }
    )
    /*this.l.getclothpage().subscribe(
      (res) => {
        console.log(res);
        this.clothes = res;
        for (let item of this.models) {
          console.log(item);
          //this.models[this.models.indexOf(item)].obj= JSON.parse(item.obj);

          this.models[this.models.indexOf(item)].description = JSON.parse(
            item.description
          );

          this.getCanvasUrl(this.models[this.models.indexOf(item)].obj, item)
            .then(async (res) => {
              await setTimeout(() => {}, 1000);
            })
            .catch((err) => {
              console.log(err);
            });
        }
      },
      (err) => {
        console.log(err);
      }
    )
*/

    this.l.gettops().subscribe(res=>{this.tops=res},err=>{console.log(err)});
  }

  async getCanvasUrl(obj: any, item: any) {
    let canvas = new fabric.Canvas(null, {
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor: 'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful: true,
      stopContextMenu: false,
    });

    console.log(item);
    return await canvas.loadFromJSON(obj, (ob: any) => {
      canvas.setHeight(400);
      canvas.setWidth(400);
      var product = {
        url: canvas.toDataURL(),
        //url2:canvas.toDataURL(),
        price: item.description.price,
        promo: item.description.promo,
        size: item.description.size,
        type: item.description.type,
        name: item.description.name,
        owner: item.description.owner,
        comment: item.description.made_with,
        item: obj,
        width: item.width,
        height: item.height,
      };
      console.log(product)
      
        this.produit.push(product);
        //this.products.push(product)
      
    });
  }

}
