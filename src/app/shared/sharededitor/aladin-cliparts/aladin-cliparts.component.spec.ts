import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinClipartsComponent } from './aladin-cliparts.component';

describe('AladinClipartsComponent', () => {
  let component: AladinClipartsComponent;
  let fixture: ComponentFixture<AladinClipartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinClipartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinClipartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
