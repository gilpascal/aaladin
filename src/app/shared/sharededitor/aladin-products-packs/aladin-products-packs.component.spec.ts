import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinProductsPacksComponent } from './aladin-products-packs.component';

describe('AladinProductsPacksComponent', () => {
  let component: AladinProductsPacksComponent;
  let fixture: ComponentFixture<AladinProductsPacksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinProductsPacksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinProductsPacksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
